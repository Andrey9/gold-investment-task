const axios = require('axios');

const { formatDateParam } = require('./helpers');

/**
 * @param {Period} period
 * @return {Promise<GoldPrice[]>}
 */
module.exports.loadPricesFor = async function loadPricesFor(period) {
  const from = formatDateParam(period.from);
  const to = formatDateParam(period.to);

  return (
    await axios.get(`http://api.nbp.pl/api/cenyzlota/${from}/${to}/`)
  ).data.map(({ data, cena }) => ({ date: data, amount: cena }));
};
