/**
 * @param {GoldPrice[]} prices
 * @return {PeriodGoldPrices}
 */
function minAndMaxPriceFor(prices) {
  /** @type number */
  let maxPriceDiff = prices[1].amount - prices[0].amount;
  /** @type number */
  let minPriceIndex = 0;
  /** @type number */
  let minPriceToReturnIndex = 0;
  /** @type number */
  let maxPriceToReturnIndex = 1;

  for (let i = 1; i < prices.length; i++) {
    if (prices[i].amount - prices[minPriceIndex].amount > maxPriceDiff) {
      maxPriceDiff = prices[i].amount - prices[minPriceIndex].amount;
      minPriceToReturnIndex = minPriceIndex;
      maxPriceToReturnIndex = i;
    }

    if (prices[i].amount < prices[minPriceIndex].amount) {
      minPriceIndex = i;
    }
  }

  return { max: prices[maxPriceToReturnIndex], min: prices[minPriceToReturnIndex] };
}

/**
 * @param {Period} period
 * @param {PricesLoader} loadPrices
 * @return {Promise<PeriodGoldPrices>}
 */
async function minAndMaxPriceForLongPeriod(period, loadPrices) {
  /** @type GoldPrice[] */
  const tempPricesArray = [];
  const oneDayMilliseconds = 24 * 60 * 60 * 1000;
  const stepMilliseconds = 365 * oneDayMilliseconds;
  /** @type Date */
  let from = period.from;
  /** @type Date */
  let to = period.to.getTime() > new Date(from.getTime() + stepMilliseconds).getTime() ?
    new Date(from.getTime() + stepMilliseconds) :
    period.to;

  while (from.getTime() < period.to.getTime()) {
    const pricesForPeriod = await loadPrices({ from, to });
    const { min, max } = minAndMaxPriceFor(pricesForPeriod);

    tempPricesArray.push(min, max);

    from = new Date(to.getTime() + oneDayMilliseconds);
    to = period.to.getTime() > new Date(from.getTime() + stepMilliseconds).getTime() ?
      new Date(from.getTime() + stepMilliseconds) :
      period.to;
  }

  return minAndMaxPriceFor(tempPricesArray);
}

module.exports.minAndMaxPriceFor = minAndMaxPriceFor;
module.exports.minAndMaxPriceForLongPeriod = minAndMaxPriceForLongPeriod;
