const { minAndMaxPriceForLongPeriod } = require('./functions');
const { loadPricesFor } = require('./loader');

(async () => {
  const beforeInvestmentAmount = 600000;
  const now = new Date();
  // calculates a date five years ago
  const startFrom = new Date(new Date().setFullYear(now.getFullYear() - 5));
  const result = await minAndMaxPriceForLongPeriod(
    { from: startFrom, to: now },
    loadPricesFor,
  );
  const afterInvestmentAmount = beforeInvestmentAmount / result.min.amount * result.max.amount;

  console.log(`Cost history from: ${startFrom.toLocaleDateString()}`);
  console.log(`Cost history to: ${now.toLocaleDateString()}`);
  console.log(`Best investment date:\t ${result.min.date} with price ${result.min.amount} PLN`);
  console.log(`Best sell date:\t\t ${result.max.date} with price ${result.max.amount} PLN`);
  console.log(`Amount before investment:\t ${beforeInvestmentAmount.toFixed(2)} PLN`);
  console.log(`Amount after investment:\t ${afterInvestmentAmount.toFixed(2)} PLN`);
  console.log(`Profit: \t\t\t ${(afterInvestmentAmount - beforeInvestmentAmount).toFixed(2)} PLN`);
})();
