/**
 * @param {Date} date
 * @return string
 */
module.exports.formatDateParam = function formatDateParam(date) {
  const offset = date.getTimezoneOffset();

  return new Date(date.getTime() - offset * 60 * 1000).toISOString().split('T')[0];
};
