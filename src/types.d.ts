type GoldPrice = {
  amount: number;
  date: string;
}

type PeriodGoldPrices = {
  min: GoldPrice;
  max: GoldPrice;
}

type Period = {
  from: Date;
  to: Date;
}

type PricesLoader = (period: Period) => Promise<GoldPrice[]>;
