const { minAndMaxPriceForLongPeriod } = require('../src/functions');
const testSet = [
  [
    { date: '2019-04-22', amount: 229.55 },
    { date: '2019-04-24', amount: 234.96 },
    { date: '2019-04-27', amount: 232.50 },
    { date: '2019-04-28', amount: 229.91 },
    { date: '2019-04-29', amount: 227.57 },
    { date: '2019-04-30', amount: 228.94 },
    { date: '2019-05-04', amount: 213.25 }, // expected total min
    { date: '2019-05-05', amount: 228.84 },
    { date: '2019-05-06', amount: 229.09 },
    { date: '2019-05-07', amount: 228.52 },
    { date: '2019-05-08', amount: 231.01 },
    { date: '2019-05-11', amount: 230.47 },
    { date: '2019-05-12', amount: 230.64 },
    { date: '2019-05-13', amount: 230.39 },
    { date: '2019-05-14', amount: 231.32 },
    { date: '2019-05-15', amount: 235.31 }, // expected max for 2019
  ],
  [
    { date: '2020-05-18', amount: 238.07 },
    { date: '2020-05-19', amount: 235.48 },
    { date: '2020-05-20', amount: 232.71 },
    { date: '2020-05-21', amount: 233.94 },
    { date: '2020-05-22', amount: 229.30 },
    { date: '2020-04-26', amount: 216.87 },
    { date: '2020-04-27', amount: 214.72 },
    { date: '2020-04-28', amount: 216.97 },
    { date: '2020-04-29', amount: 216.18 },
    { date: '2020-04-30', amount: 214.13 },
    { date: '2020-05-04', amount: 214.51 },
    { date: '2020-05-05', amount: 240.29 }, // expected max for 2020
    { date: '2020-05-06', amount: 218.52 },
    { date: '2020-05-07', amount: 221.92 },
  ],
  [
    { date: '2021-05-10', amount: 223.57 },
    { date: '2021-05-11', amount: 221.85 },
    { date: '2021-05-12', amount: 220.62 },
    { date: '2021-05-13', amount: 220.47 },
    { date: '2021-05-14', amount: 250.66 }, // expected total max
    { date: '2021-05-17', amount: 220.88 },
    { date: '2021-05-18', amount: 222.24 },
    { date: '2021-05-19', amount: 222.37 },
    { date: '2021-05-20', amount: 225.19 },
    { date: '2021-05-21', amount: 223.91 },
    { date: '2021-05-23', amount: 217.76 },
  ],
];

const prepareLoadPricesMock = function () {
  /** @type GoldPrice[][] */
  const date = [...testSet];

  return async function (period) {
    const result = date.shift();

    return Promise.resolve(result);
  };
};

test.each([
  {
    period: {
      from: new Date('2019-01-01'),
      to: new Date('2019-11-29'),
    },
    expected: {
      min: { date: '2019-05-04', amount: 213.25 },
      max: { date: '2019-05-15', amount: 235.31 },
    },
  },
  {
    period: {
      from: new Date('2019-01-01'),
      to: new Date('2020-11-29'),
    },
    expected: {
      min: { date: '2019-05-04', amount: 213.25 },
      max: { date: '2020-05-05', amount: 240.29 },
    },
  },
  {
    period: {
      from: new Date('2019-01-01'),
      to: new Date('2021-11-29'),
    },
    expected: {
      min: { date: '2019-05-04', amount: 213.25 },
      max: { date: '2021-05-14', amount: 250.66 },
    },
  },
])
(
  'should find min and max prices where max price is set later than min price in chunked array',
  async ({ period, expected }) => {
    expect(
      await minAndMaxPriceForLongPeriod(
        period,
        prepareLoadPricesMock(),
      ),
    ).toEqual(expected);
  });
