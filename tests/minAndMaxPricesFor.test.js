const { minAndMaxPriceFor } = require('../src/functions');

test.each([
  {
    prices: [
      { amount: 1, date: '2021-01-01' },
      { amount: 2, date: '2021-01-02' },
      { amount: 90, date: '2021-01-03' },
      { amount: 10, date: '2021-01-04' },
      { amount: 110, date: '2021-01-05' },
    ],
    expected: {
      min: { amount: 1, date: '2021-01-01' },
      max: { amount: 110, date: '2021-01-05' },
    },
  },
  {
    prices: [
      { amount: 20, date: '2021-01-01' },
      { amount: 2, date: '2021-01-02' },
      { amount: 3, date: '2021-01-03' },
      { amount: 10, date: '2021-01-04' },
      { amount: 6, date: '2021-01-05' },
      { amount: 4, date: '2021-01-06' },
      { amount: 8, date: '2021-01-07' },
      { amount: 1, date: '2021-01-08' },
    ],
    expected: {
      min: { amount: 2, date: '2021-01-02' },
      max: { amount: 10, date: '2021-01-04' },
    },
  },
  {
    prices: [
      { amount: 80, date: '2021-01-01' },
      { amount: 2, date: '2021-01-02' },
      { amount: 6, date: '2021-01-03' },
      { amount: 300, date: '2021-01-04' },
      { amount: 100, date: '2021-01-05' },
    ],
    expected: {
      min: { amount: 2, date: '2021-01-02' },
      max: { amount: 300, date: '2021-01-04' },
    },
  },
])('should find min and max prices where max price is set later than min price', ({ prices, expected }) => {
  expect(minAndMaxPriceFor(prices)).toEqual(expected);
});
