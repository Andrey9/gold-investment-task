const { formatDateParam } = require('../src/helpers');

test.each([
  { date: new Date('2022-01-01'), expectedResult: '2022-01-01' },
  { date: new Date('2021-12-01'), expectedResult: '2021-12-01' },
  { date: new Date('2022-01-31'), expectedResult: '2022-01-31' },
  { date: new Date('2021-12-31'), expectedResult: '2021-12-31' },
])('should format date to string with format yyyy-mm-dd', ({ date, expectedResult }) => {
  expect(formatDateParam(date)).toEqual(expectedResult);
});
